import java.io.*;
import java.util.*;
import java.math.*;

public class Practice {
	private double[] studentScores;
	private int quanOfStudents;

	public Practice() {

		Scanner scanner = new Scanner(System.in);
		System.out.println("Would you like to load in scores?");
		String loadScores = scanner.nextLine();
		int i = 0;
		if (loadScores.toLowerCase().contains("y")) {
			loadPreviousList2();
		} else {
			System.out.println("Enter quantity of students to do stuff with scores.");
			this.quanOfStudents = scanner.nextInt();
			studentScores = new double[quanOfStudents];
			while (i < this.quanOfStudents) {
				System.out.println("Enter grade for student " + (i + 1));
				this.studentScores[i] = scanner.nextDouble();

				i++;
			}
			writeOutList();
		}

		double sumOfScores = 0;
		String studentScoreMsg = "";
		double highestScore = 0;
		double lowestScore = this.studentScores[0];// Setting it to the first score as zero will likely always be lower.
		i = 0;
		while (i < this.quanOfStudents) {
			sumOfScores += studentScores[i];

			studentScoreMsg += this.studentScores[i] + " ";
			highestScore = Math.max(this.studentScores[i], highestScore);
			lowestScore = Math.min(this.studentScores[i], lowestScore);
			i++;
		}
		double average = sumOfScores / this.quanOfStudents;
		System.out.println("Student scores as obtained:" + studentScoreMsg);
		System.out.printf("Total of all scores: %.2f\n", sumOfScores);
		System.out.printf("Average of " + this.quanOfStudents + " students : %.2f\n", average);
		System.out.printf("The highest score achieved : %.2f\n", highestScore);
		System.out.printf("The lowest score achieved : %.2f\n", lowestScore);
	}

	public void loadPreviousList() { //THIS ONE DOESN"T WORK BUT KEPT IT HERE SO I KNOW WHAT NOT TO DO.
		BufferedReader inFile = null;
		BufferedReader loadFile = null;
		int lineNum = 0;
		int totalLines = 0;

		try {
			loadFile = new BufferedReader(new FileReader("./StudentScores.txt"));
			String currLine = loadFile.readLine();
			while (currLine != null) {
				this.quanOfStudents++;
				currLine = loadFile.readLine();
				totalLines++;
			}
			this.studentScores = new double[this.quanOfStudents];
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		try {
			inFile = new BufferedReader(new FileReader("./StudentScores.txt"));
			Scanner scanner = new Scanner(inFile);
			String currLine1 = scanner.nextLine();
			lineNum = 0;
			while (currLine1 != null) {
				this.studentScores[lineNum] = Integer.parseInt(currLine1);
				lineNum++;
				currLine1 = scanner.nextLine();
			}
			inFile.close();
		} catch (

		Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void loadPreviousList2() {
		BufferedReader inFile = null;
		BufferedReader loadFile = null;
		
		int lineNum = 0;

		try { inFile = new BufferedReader(new FileReader("./StudentScores.txt")); } catch (Exception e) { return; }
        try { loadFile = new BufferedReader( inFile ); } catch (Exception e) { return; }
        
        Scanner scanner = new Scanner(loadFile);
        
        while (scanner.hasNextLine()) {
        	scanner.nextLine();
        	this.quanOfStudents++;
        }
        scanner.close();
        
        try { inFile = new BufferedReader(new FileReader("./StudentScores.txt")); } catch (Exception e) { return; }
        try { loadFile = new BufferedReader( inFile ); } catch (Exception e) { return; }
        
        scanner = new Scanner(loadFile);
        
        this.studentScores = new double[this.quanOfStudents]; 
        
        lineNum = 0;
     
        while ( lineNum < this.quanOfStudents) {
        	this.studentScores[lineNum] = Double.parseDouble(scanner.nextLine());
        	lineNum++;
        }
        scanner.close();
	}
	
	public void writeOutList() {
		BufferedWriter outFile = null;
		try {
			outFile = new BufferedWriter(new FileWriter("./StudentScores.txt"));
			int i = 0;
			while (i < this.studentScores.length) {
				outFile.write(this.studentScores[i] + "\n");
				i++;
			}
			outFile.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public static void main(String[] args) {
		Practice prac = new Practice();
	}
}
